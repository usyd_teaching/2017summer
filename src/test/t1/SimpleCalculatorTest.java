package t1;

import static org.junit.Assert.*;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class SimpleCalculatorTest {


	@Test
	public void testZeroes(){
		assertEquals(0, SimpleCalculator.add(0, 0));
		assertEquals(0, SimpleCalculator.subtract(0, 0));
		assertEquals(0, SimpleCalculator.multiply(0, 0));
		assertEquals(0, SimpleCalculator.divide(0, 10), 0.01);
	}
	
	@Test
	public void testAdd(){
		assertEquals(4, SimpleCalculator.add(1, 3));
		assertEquals(4, SimpleCalculator.add(3, 1));
		assertFalse(SimpleCalculator.add(4, 1) == 4);
	}
	
	@Test
	public void testSubtract(){
		assertEquals(2, SimpleCalculator.subtract(3, 1));
		assertEquals(-2, SimpleCalculator.subtract(1, 3));
		assertTrue( SimpleCalculator.subtract(10, 4) > 0);
	}
	
	@Test
	public void testMultiply(){
		assertEquals(3, SimpleCalculator.multiply(3, 1));
		assertEquals(3, SimpleCalculator.multiply(1, 3));
		assertEquals(0, SimpleCalculator.multiply(1, 0));
		
		assertArrayEquals(new int[]{2,4,6}, new int[]{
				SimpleCalculator.multiply(1,2),
				SimpleCalculator.multiply(2,2),
				SimpleCalculator.multiply(3,2),
		});
	}
	
	@Rule
	public ExpectedException ex = ExpectedException.none();
	
	@Test
	public void testDivide(){
		double epsilon = 0.01;
		assertEquals(3, SimpleCalculator.divide(3, 1), epsilon);
		
		assertEquals(0.33333, SimpleCalculator.divide(1, 3), epsilon);
	
		ex.expect(ArithmeticException.class);
		assertEquals(0, SimpleCalculator.divide(1, 0), epsilon);
	
	}
	
	
}
