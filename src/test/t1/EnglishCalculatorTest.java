package t1;

import static org.junit.Assert.*;

import org.junit.Test;

public class EnglishCalculatorTest {

	@Test
	public void testEnglishCalculate() {
		assertEquals(20.0, EnglishCalculator.calculate("4 times 5"), 0.01);
		assertEquals(12.0, EnglishCalculator.calculate("2 plus 10"), 0.01);
		assertEquals(-1.0, EnglishCalculator.calculate("6 minus 7"), 0.01);
		assertEquals(3.0, EnglishCalculator.calculate("15   divided by 5"), 0.01);
	}

}
