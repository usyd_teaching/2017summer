package t3;
import static org.junit.Assert.assertEquals;

import java.util.EmptyStackException;

import org.junit.Test;

public class PalindromesTest {

    @Test
    public void testCase() throws EmptyStackException{
        assertEquals(true, Palindromes.isPalindrome("EyE"));
        assertEquals(false, Palindromes.isPalindrome("Eye"));
    }
    @Test
    public void testBasicNot() throws EmptyStackException{
        assertEquals(false, Palindromes.isPalindrome("hello"));
        assertEquals(false, Palindromes.isPalindrome("book"));
    }
    @Test
    public void testBasic() throws EmptyStackException{
        assertEquals(true, Palindromes.isPalindrome("eye"));
        assertEquals(true, Palindromes.isPalindrome("racecar"));
        assertEquals(true, Palindromes.isPalindrome("glenelg"));
    }
    @Test
    public void testEmptyString() throws EmptyStackException{
        assertEquals(true, Palindromes.isPalindrome(""));
    }

    @Test
    public void testSentence() throws EmptyStackException{
        assertEquals(true, Palindromes.isPalindromeSentence("eye"));
        assertEquals(true, Palindromes.isPalindromeSentence("racecar"));
        assertEquals(false, Palindromes.isPalindromeSentence("hello"));
        assertEquals(false, Palindromes.isPalindromeSentence("book"));
        assertEquals(true, Palindromes.isPalindromeSentence("Eye"));
        assertEquals(true, Palindromes.isPalindromeSentence("Madam, I'm Adam"));
        assertEquals(true, Palindromes.isPalindromeSentence("Never odd or even"));
        assertEquals(false, Palindromes.isPalindromeSentence("Hello world"));
    }
    @Test
    public void testEmptySentence() throws EmptyStackException{
        assertEquals(true, Palindromes.isPalindromeSentence(""));
    }
}
