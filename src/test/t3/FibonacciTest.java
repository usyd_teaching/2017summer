package t3;

import static org.junit.Assert.assertEquals;

import java.util.EmptyStackException;
import java.util.Stack;

import org.junit.Test;

public class FibonacciTest {
	
	private <E> String stackToString(Stack<E> stack) {
		StringBuilder buff = new StringBuilder();
		buff.append(")");
		while(!stack.isEmpty()) {
			E e = stack.pop();
			buff.insert(0, e.toString());
			buff.insert(0, ",");
		}
		buff.deleteCharAt(0); //remove ','
		buff.insert(0, "(");
		return buff.toString();
	}

    @Test
    public void testOneToFive() throws EmptyStackException{
        Stack<Integer> s = Fibonacci.getNumbers(1);
        assertEquals("(0,1)", stackToString(s));
        s = Fibonacci.getNumbers(2);
        assertEquals("(0,1,1)", stackToString(s));
        s = Fibonacci.getNumbers(3);
        assertEquals("(0,1,1,2)", stackToString(s));
        s = Fibonacci.getNumbers(4);
        assertEquals("(0,1,1,2,3)", stackToString(s));
        s = Fibonacci.getNumbers(5);
        assertEquals("(0,1,1,2,3,5)", stackToString(s));
    }

    @Test
    public void testStress() throws EmptyStackException{
        Stack<Integer> s = Fibonacci.getNumbers(1000);
        for(int i=0; i<995; i++){
            s.pop();
        }
        assertEquals("(0,1,1,2,3,5)", stackToString(s));
    }

    @Test
    public void testNegative() throws EmptyStackException{
        Stack<Integer> s = Fibonacci.getNumbers(-1);
        assertEquals(true, s.isEmpty());
    }

}
