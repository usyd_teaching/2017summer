package t5;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

import t4.binarytree.BTNode;
import t4.binarytree.SimpleBTNode;
import t4.binarytree.SimpleBinaryTree;

public class BinaryTreeAlgosTest {

	SimpleBinaryTree<Integer> t; 
	BTNode<Integer> five = new SimpleBTNode<>(5);
	BTNode<Integer> three = new SimpleBTNode<>(3, five);
	BTNode<Integer> seven = new SimpleBTNode<>(7, five);
	BTNode<Integer> two = new SimpleBTNode<>(2, three);
	BTNode<Integer> four = new SimpleBTNode<>(4, three);
	BTNode<Integer> six = new SimpleBTNode<>(6, seven);
	BTNode<Integer> eleven = new SimpleBTNode<>(11, seven);
	BTNode<Integer> nine = new SimpleBTNode<>(9, eleven);
	BTNode<Integer> ten = new SimpleBTNode<>(10, eleven);

	@Before
	public void setup() {

		five.setLeft(three);
		five.setRight(seven);
		three.setLeft(two);
		three.setRight(four);

		seven.setLeft(six);
		seven.setRight(eleven);
		eleven.setLeft(nine);
		eleven.setRight(ten);

		t = new SimpleBinaryTree<>();
		t.setRoot(five);
	}

	@Test
	public void testParenthetic() throws Exception {
		assertEquals("5(3(2, 4), 7(6, 11(9, 10)))", BinaryTreeAlgos.toParentheticString(t));
	}

	@Test
	public void testNodeCoord() {
		assertEquals( Arrays.asList(
				new BinaryTreeAlgos.Coordinate(1,2, two),
				new BinaryTreeAlgos.Coordinate(2,1, three),
				new BinaryTreeAlgos.Coordinate(3,2, four),
				new BinaryTreeAlgos.Coordinate(4,0, five),
				new BinaryTreeAlgos.Coordinate(5,2, six),
				new BinaryTreeAlgos.Coordinate(6,1, seven),
				new BinaryTreeAlgos.Coordinate(7,3, nine),
				new BinaryTreeAlgos.Coordinate(8,2, eleven),
				new BinaryTreeAlgos.Coordinate(9,3, ten)),
				BinaryTreeAlgos.getNodeCoordinates(t));
	}

	@Test
	public void testCountLeaves() throws Exception {
		assertEquals(5, BinaryTreeAlgos.countLeaves(t));
	}

	@Test
	public void testHasTwoChildrenOrNone() throws Exception {
		assertTrue( BinaryTreeAlgos.twoChildrenOrNone(t));
	}

	@Test
	public void testTrim() {
		BinaryTreeAlgos.trim(t);
		assertEquals("5(3, 7(, 11))", BinaryTreeAlgos.toParentheticString(t));
	}
}
