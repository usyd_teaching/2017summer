package t13;

import static org.junit.Assert.*;

import org.junit.Test;

import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.IntStream;

public class QuickselectTest {

	
	Integer[] SMALL = {1,4,6,8,2,3,7,5,9};
	
	@Test
	public void testSmall() throws Exception {
		for(int k=1; k<10; k++) {
			assertEquals(new Integer(k), Quickselect.quickselect(k, SMALL));
		}
	}
	
	@Test
	public void test1k() throws Exception {
//		Integer[] oneK = IntStream.generate( () -> 0 + (int)(1000*Math.random() + 1) )
		Integer[] oneK = ThreadLocalRandom.current().ints()
				.boxed()
				.limit(100)
				.toArray(Integer[]::new);
		
		Integer[] check = Arrays.copyOf(oneK, oneK.length);
		Arrays.sort(check);
		
		for(int k=1; k<=check.length; k++) {
			assertEquals("value of k: " + k, new Integer(check[k-1]), Quickselect.quickselect(k, oneK));
		}
	}
}
