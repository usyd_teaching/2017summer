package t11;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

public class TravelDestinationsTest {
	@Test
	public void testSmall() throws Exception {
		TravelDestinations td = new TravelDestinations();
		td.addFlight("Au", "US");
		td.addFlight("Au", "UK");
		td.addFlight("UK", "Fr");
		td.addFlight("UK", "Au");
		td.addFlight("US", "Fr");
		td.addFlight("US", "Gb");
	
		List<String> neighbours = td.getAvailableCountries("Au");
        assertEquals(2, neighbours.size());
        assertTrue(neighbours.contains("US"));
        assertTrue(neighbours.contains("UK"));
	}
}
