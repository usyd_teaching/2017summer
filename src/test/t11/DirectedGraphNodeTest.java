package t11;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;

public class DirectedGraphNodeTest {

	@Test
	public void testConstruction() throws Exception {
		Graph<Integer> g = new DirectedGraph<>();
		assertTrue(g.isEmpty());
		assertEquals(0, g.size());
		assertTrue(g.getNodes().isEmpty());
	}
	
	@Test
	public void testSize_and_IsEmpty() throws Exception {
		
		Graph<String> g = new DirectedGraph<>();
		assertEquals(0, g.size());
		assertTrue(g.isEmpty());
		
        Node<String> A = new DirectedGraphNode<>("A");
        g.addNode(A);
		assertEquals(1, g.size());
		assertFalse(g.isEmpty());
		
        Node<String> B = new DirectedGraphNode<>("B");
		g.addNode(B);
		assertEquals(2, g.size());
		assertFalse(g.isEmpty());

        Node<String> C = new DirectedGraphNode<>("C");
        g.addNode(C);
		assertEquals(3, g.size());
		assertFalse(g.isEmpty());
		
		g.removeNode(A);
		assertEquals(2, g.size());
		assertFalse(g.isEmpty());
		
		g.removeNode(C);
		assertEquals(1, g.size());
		assertFalse(g.isEmpty());
		
		g.removeNode(A);
		assertEquals(1, g.size());
		assertFalse(g.isEmpty());
		
		g.removeNode(B);
		assertEquals(0, g.size());
		assertTrue(g.isEmpty());
	}
	
	@Test
	public void testAddNode() throws Exception {
		
		Graph<String> g = new DirectedGraph<>();
		
        Node<String> A = new DirectedGraphNode<>("A");
        g.addNode(A);
		assertThat(g.getNodes(), hasItems(A));
        
        Node<String> B = new DirectedGraphNode<>("B");
		g.addNode(B);
		assertThat(g.getNodes(), hasItems(A,B));

        Node<String> C = new DirectedGraphNode<>("C");
		g.addNode(C);
		assertThat(g.getNodes(), hasItems(A,B,C));
	}
	
	@Test
	public void testRemoveNode() throws Exception {
		

		Graph<String> g = new DirectedGraph<>();
		
        Node<String> A = new DirectedGraphNode<>("A");
        
        Node<String> B = new DirectedGraphNode<>("B");

        Node<String> C = new DirectedGraphNode<>("C");
		g.addNode(A);
		g.addNode(B);
		g.addNode(C);
        assertEquals(Arrays.asList(A,B,C), g.getNodes());
        
        g.removeNode(B);
        assertThat(g.getNodes(), hasItems(C,A));
        
        
        g.removeNode(B);
        assertThat(g.getNodes(), hasItems(C,A));
        
        g.removeNode(A);
        g.removeNode(C);
        assertTrue(g.getNodes().isEmpty());
	}
	
	@Test
	public void testSmallGraph() throws Exception {
		Graph<String> g = new DirectedGraph<>();
        Node<String> A = new DirectedGraphNode<>("A");
        Node<String> B = new DirectedGraphNode<>("B");
        Node<String> C = new DirectedGraphNode<>("C");
        Node<String> D = new DirectedGraphNode<>("D");
        Node<String> E = new DirectedGraphNode<>("E");
        
        g.addNode(A);
        g.addNode(B);
        g.addNode(C);
        g.addNode(D);
        g.addNode(E);
        
        g.addEdge(A, B);
        g.addEdge(A, C);
        g.addEdge(B, D);
        g.addEdge(C, D);
        g.addEdge(D, E);
        
        assertThat(A.getNeighbours(), hasItems(B,C));
        assertThat(B.getNeighbours(), hasItems(D));
        assertThat(C.getNeighbours(), hasItems(D));
        assertThat(D.getNeighbours(), hasItems(E));
        assertTrue(E.getNeighbours().isEmpty());
	}
}
