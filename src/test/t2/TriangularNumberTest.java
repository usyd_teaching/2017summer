package t2;

import static org.junit.Assert.*;

import org.junit.Test;

public class TriangularNumberTest {

	@Test
	public void testSmallTriangles() {
	    assertEquals(1, TriangularNumber.triangular(1));
	    assertEquals(3, TriangularNumber.triangular(2));
	    assertEquals(6, TriangularNumber.triangular(3));
	}         
	
	@Test
	public void testZeroTriangle() {
	    assertEquals(0, TriangularNumber.triangular(0));
	}        
}
