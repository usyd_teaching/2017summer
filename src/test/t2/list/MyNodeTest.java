package t2.list;

import static org.junit.Assert.*;

import org.junit.Test;

public class MyNodeTest {

	@Test
	public void testConstruction(){
		String s = "hello";
		MyNode<String> node = new MyNode<>(s);
		assertEquals(s, node.getElement());
		assertSame(s,node.getElement());
	}

	@Test
	public void testSetElement(){
		String s = "hello";
		MyNode<String> node = new MyNode<>(null);
		assertNull(node.getElement());
		node.setElement(s);
		assertEquals("hello", node.getElement()); //assertEquals uses String.equals, which is why this passes 
		assertEquals(s, node.getElement()); // hence this test may not be strictly testing for the same object
		assertSame(s, node.getElement()); // this tests for the same object
	}

	@Test
	public void testSetNext(){
		String s = "hello";
		MyNode<String> node = new MyNode<>(s);
		String w = "world";
		MyNode<String> node2 = new MyNode<>(w);
		node.setNext(node2);
		assertEquals(node2, node.getNext());
	}
	
	@Test
	public void testChaining(){
		String s = "hello";
		MyNode<String> node = new MyNode<>(s);
		node.setNext(node);
		assertEquals(node, node.getNext());
		assertEquals(node, node.getNext().getNext());
		assertEquals(node, node.getNext().getNext().getNext());
	}
}
