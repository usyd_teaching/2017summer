package t2.list;

import static org.junit.Assert.*;

import org.junit.Test;

public class MyLinkedListTest {

	@Test
	public void testConstruction() {
		List<String> l = new MyLinkedList<>();
		assertEquals(0, l.size());
		assertTrue(l.isEmpty());
		assertNull(l.get(0));
	}
	
	@Test
	public void testSize() {
		
		List<String> l = new MyLinkedList<>();
		l.add("hello");
		assertEquals(1, l.size());
		l.add("world");
		assertEquals(2, l.size());
		assertFalse(l.isEmpty());
		l.remove(l.get(0));
		assertEquals(1, l.size());
		l.remove(l.get(1));
		assertEquals(1, l.size());
		l.remove(l.get(0));
		assertEquals(0, l.size());
		assertTrue(l.isEmpty());
	}
	
	
	@Test
	public void testGet() {
		String a = "hello", b = "world", c = "today";
		List<String> l = new MyLinkedList<>();
		l.add(a);
		l.add(b);
		l.add(c);
		
		assertNull(l.get(-1));
		assertNull(l.get(3));
		
		assertSame(b, l.get(1));
		assertSame(c, l.get(2));
		assertSame(a, l.get(0));
		
	}
	
	@Test
	public void testRemove() {
		String a = "hello", b = "world", c = "today";
		List<String> l = new MyLinkedList<>();
		l.add(a);
		l.add(b);
		l.add(c);
		
		assertEquals(3, l.size());
		
		l.remove(new String("hello")); //in case the JVM caches strings
		assertEquals(3, l.size());
		
		l.remove(a);
		assertSame(b, l.get(0));
		
		l.remove(c);
		assertSame(b, l.get(0));
		
		l.remove(b);
		assertEquals(0, l.size());
		assertNull(l.get(0));
	}

}
