package t4.familytree;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;

import org.junit.Before;
import org.junit.Test;

import t4.Node;
import t4.SimpleNode;
import t4.SimpleTree;
import t4.Tree;

public class FamilyTreeTest {

	private FamilyTree f;
	private Node<String> bob;
	private Node<String> jane;
	private Node<String> tim;
	private Node<String> kate;

	@Before
	public void setup() {
		
		Tree<String> t = new SimpleTree<>();

		this.bob = new SimpleNode<>("Bob");
		this.jane = new SimpleNode<>("Jane");
		this.tim = new SimpleNode<>("Tim");
		this.kate = new SimpleNode<>("Kate");

		t.setRoot(bob);
		t.insert(bob, jane);
		t.insert(bob, tim);
		t.insert(tim, kate);

		this.f = new FamilyTree(t);
	}

	@Test
	public void testGetParent() throws Exception {
		assertNull(f.getParent(bob));
		assertSame("Bob", f.getParent(jane));
		assertSame("Bob", f.getParent(tim));
		assertSame("Tim", f.getParent(kate));
	}
	
	@Test
	public void testGetGrandparent() throws Exception {
		assertNull(f.getGrandparent(bob));
		assertNull(f.getGrandparent(jane));
		assertNull(f.getGrandparent(tim));
		assertSame("Bob", f.getGrandparent(kate));
	}
	
	@Test
	public void testGetChildren() {
		assertEquals(Arrays.asList(), f.getChildren(kate));
		assertEquals(Arrays.asList(), f.getChildren(jane));
		assertEquals(Arrays.asList("Kate"), f.getChildren(tim));
		assertEquals(new HashSet<>(Arrays.asList("Tim", "Jane")), new HashSet<>(f.getChildren(bob)));
//		assertEquals((Arrays.asList("Tim", "Jane")), (f.getChildren(bob))); //this fails; why?
	}
	
	@Test
	public void testGetGrandchildren() throws Exception {
		assertEquals(Arrays.asList(), f.getGrandchildren(kate));
		assertEquals(Arrays.asList(), f.getGrandchildren(jane));
		assertEquals(Arrays.asList(), f.getGrandchildren(tim));
		assertEquals(Arrays.asList("Kate"), f.getGrandchildren(bob));
	}
	
	@Test
	public void testGetSiblings() {
		
		assertEquals(Arrays.asList(), f.getSiblings(kate));
		assertEquals(Arrays.asList(), f.getSiblings(bob));
		assertEquals(Arrays.asList("Tim"), f.getSiblings(jane));
		assertEquals(Arrays.asList("Jane"), f.getSiblings(tim));
	}
}
