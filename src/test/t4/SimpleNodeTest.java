package t4;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

public class SimpleNodeTest {

	@Test
	public void testConstruction() {
		Node<Integer> n = new SimpleNode<>(3);
		assertEquals(null, n.getParent());
		assertEquals(new ArrayList<Node<Integer>>(), n.getChildren());
		assertEquals(3, n.getElement().intValue());
	}
	
	@Test
	public void testSetParent() {
		Node<Integer> n = new SimpleNode<>(3);
		assertEquals(null, n.getParent());
		Node<Integer> paren = new SimpleNode<>(1);
		n.setParent(paren);
		assertSame(paren, n.getParent());
		assertFalse(paren.getChildren().contains(n)); //spec requires no change to parent's children
	}
	
	@Test
	public void testSetElement() {
		Node<Integer> n = new SimpleNode<>(3);
		assertEquals(3, n.getElement().intValue());
		n.setElement(5);
		assertEquals(5, n.getElement().intValue());
	}

	@Test
	public void testSetChildren() {
		Node<Integer> n3 = new SimpleNode<>(3);
		Node<Integer> n5 = new SimpleNode<>(3);
		Node<Integer> paren = new SimpleNode<>(1);
		paren.addChild(n3);
		assertTrue(paren.getChildren().contains(n3));
		assertFalse(paren.getChildren().contains(n5));
		paren.addChild(n5);
		assertTrue(paren.getChildren().contains(n3));
		assertTrue(paren.getChildren().contains(n5));
		
		paren.removeChild(n3);
		assertTrue(paren.getChildren().contains(n5));
		assertFalse(paren.getChildren().contains(n3));
		
		paren.removeChild(n5);
		assertFalse(paren.getChildren().contains(n5));
		assertFalse(paren.getChildren().contains(n3));
		assertEquals(new ArrayList<Node<Integer>>(), paren.getChildren());
	}

}
