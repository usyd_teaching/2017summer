package t4.binarytree;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import t4.binarytree.BTNode;
import t4.binarytree.SimpleBTNode;
import t4.binarytree.SimpleBinaryTree;

public class SimpleBinaryTreeTest {


	SimpleBinaryTree<Integer> t; 
	BTNode<Integer> five = new SimpleBTNode<>(5);
	BTNode<Integer> three = new SimpleBTNode<>(3, five);
	BTNode<Integer> seven = new SimpleBTNode<>(7, five);
	BTNode<Integer> two = new SimpleBTNode<>(2, three);
	BTNode<Integer> four = new SimpleBTNode<>(4, three);
	BTNode<Integer> six = new SimpleBTNode<>(6, seven);
	BTNode<Integer> eleven = new SimpleBTNode<>(11, seven);
	BTNode<Integer> nine = new SimpleBTNode<>(9, eleven);
	BTNode<Integer> ten = new SimpleBTNode<>(10, eleven);

	@Before
	public void setup() {

		five.setLeft(three);
		five.setRight(seven);
		three.setLeft(two);
		three.setRight(four);

		seven.setLeft(six);
		seven.setRight(eleven);
		eleven.setLeft(nine);
		eleven.setRight(ten);

		t = new SimpleBinaryTree<>();
		t.setRoot(five);
	}

	@Test
	public void testConstruction() {
		SimpleBinaryTree<String> t = new SimpleBinaryTree<>();
		assertEquals(0, t.size());
		assertTrue(t.isEmpty());
	}

	@Test
	public void testGetSize() throws Exception {
		SimpleBinaryTree<String> t = new SimpleBinaryTree<>();

		BTNode<String> A = new SimpleBTNode<>("A");
		BTNode<String> B = new SimpleBTNode<>("B");
		BTNode<String> C = new SimpleBTNode<>("C");

		t.setRoot(A);
		assertEquals(1,t.size());
		assertFalse(t.isEmpty());
		t.insert(A, B);
		assertEquals(2,t.size());
		t.insert(A, C);	
		assertEquals(3, t.size());
		assertFalse(t.isEmpty());

		t.remove(B);
		assertEquals(2, t.size());
		assertFalse(t.isEmpty());
		t.remove(C);
		assertEquals(1, t.size());
		assertFalse(t.isEmpty());
		t.remove(A);
		assertEquals(0, t.size());
		assertTrue(t.isEmpty());	      

		t.setRoot(A);
		t.insert(A, B);
		t.insert(A, C);
		t.remove(A);
		assertEquals(0, t.size());
		assertTrue(t.isEmpty());	      
	}

	@Test
	public void testSetRoot() throws Exception {

		SimpleBinaryTree<String> t = new SimpleBinaryTree<>();

		BTNode<String> A = new SimpleBTNode<>("A");
		BTNode<String> B = new SimpleBTNode<>("B");
		BTNode<String> C = new SimpleBTNode<>("C");


		t.setRoot(A);	    		 
		assertSame(A, t.getRoot());
		t.setRoot(B);
		assertSame(B, t.getRoot());
		t.setRoot(C);
		assertSame(C, t.getRoot());
	}
}
