package t4;

import static org.junit.Assert.*;

import org.junit.Test;

public class SimpleTreeTest {

	@Test
	public void testConstruction() {
		Tree<String> t = new SimpleTree<>();
		assertEquals(0, t.size());
		assertTrue(t.isEmpty());
	}

	@Test
	public void testGetSize() throws Exception {
	      Tree<String> t = new SimpleTree<>();

	      Node<String> A = new SimpleNode<>("A");
	      Node<String> B = new SimpleNode<>("B");
	      Node<String> C = new SimpleNode<>("C");

	      t.setRoot(A);
	      assertEquals(1,t.size());
	      assertFalse(t.isEmpty());
	      t.insert(A, B);
	      assertEquals(2,t.size());
	      t.insert(A, C);	
	      assertEquals(3, t.size());
	      assertFalse(t.isEmpty());

	      t.remove(B);
	      assertEquals(2, t.size());
	      assertFalse(t.isEmpty());
	      t.remove(C);
	      assertEquals(1, t.size());
	      assertFalse(t.isEmpty());
	      t.remove(A);
	      assertEquals(0, t.size());
	      assertTrue(t.isEmpty());	      

	      t.setRoot(A);
	      t.insert(A, B);
	      t.insert(A, C);
	      t.remove(A);
	      assertEquals(0, t.size());
	      assertTrue(t.isEmpty());	      
	}

	@Test
	public void testSetRoot() throws Exception {
		
	     Tree<String> t = new SimpleTree<>();

	      Node<String> A = new SimpleNode<>("A");
	      Node<String> B = new SimpleNode<>("B");
	      Node<String> C = new SimpleNode<>("C");

	      t.setRoot(A);	    		 
	      assertSame(A, t.getRoot());
	      t.setRoot(B);
	      assertSame(B, t.getRoot());
	      t.setRoot(C);
	      assertSame(C, t.getRoot());
	}
}
