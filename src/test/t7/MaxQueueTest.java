package t7;

import static org.junit.Assert.*;

import org.junit.Test;

public class MaxQueueTest {

	@Test
	public void testConstruction() throws Exception {
		MaxQueue<Integer> pq = new MaxQueue<>();
		assertEquals(0, pq.size());
		assertTrue(pq.isEmpty());
	}
	
	@Test
	public void testRemoveMax() throws Exception {
		
		MaxQueue<Integer> pq = new MaxQueue<>();
		pq.add(1);
		pq.add(5);
		pq.add(4);
		pq.add(-1);
		
		assertEquals(new Integer(5), pq.removeMax());
		assertEquals(new Integer(4), pq.removeMax());
		assertEquals(new Integer(1), pq.removeMax());
		assertEquals(new Integer(-1), pq.removeMax());
	}

	@Test
	public void testMin() throws Exception {
		
		MaxQueue<Integer> pq = new MaxQueue<>();
		pq.add(1);
		pq.add(5);
		pq.add(4);
		pq.add(-1);
		
		assertEquals(new Integer(5), pq.max());
		pq.removeMax();
		assertEquals(new Integer(4), pq.max());
		pq.removeMax();
		assertEquals(new Integer(1), pq.max());
		pq.removeMax();
		assertEquals(new Integer(-1), pq.max());
		pq.removeMax();
	}
	
	@Test
	public void testSize() throws Exception {
		
		MaxQueue<Integer> pq = new MaxQueue<>();
		pq.add(1);
		pq.add(5);
		pq.add(4);
		pq.add(-1);
		
		assertEquals(4, pq.size());
		assertFalse(pq.isEmpty());

		pq.removeMax();
		assertEquals(3, pq.size());
		assertFalse(pq.isEmpty());
		
		pq.removeMax();
		assertEquals(2, pq.size());
		assertFalse(pq.isEmpty());
		
		pq.removeMax();
		assertEquals(1, pq.size());
		assertFalse(pq.isEmpty());

		pq.removeMax();
		assertEquals(0, pq.size());
		assertTrue(pq.isEmpty());
		
	}
}
