package t7;

import static org.junit.Assert.*;

import org.junit.Test;

public class ArrayListPQTest {

	@Test
	public void testConstruction() throws Exception {
		PriorityQueue<Integer,String> pq = new ArrayListPQ<>();
		assertEquals(0, pq.size());
		assertTrue(pq.isEmpty());
	}
	
	@Test
	public void testRemoveMin() throws Exception {
		
		PriorityQueue<Integer,String> pq = new ArrayListPQ<>();
		pq.insert(1, "B");
		pq.insert(5, "D");
		pq.insert(4, "C");
		pq.insert(-1, "A");
		
		assertEquals("A", pq.removeMin());
		assertEquals("B", pq.removeMin());
		assertEquals("C", pq.removeMin());
		assertEquals("D", pq.removeMin());
	}

	@Test
	public void testMin() throws Exception {
		
		PriorityQueue<Integer,String> pq = new ArrayListPQ<>();
		pq.insert(1, "B");
		pq.insert(5, "D");
		pq.insert(4, "C");
		pq.insert(-1, "A");
		
		assertEquals("A", pq.min());
		pq.removeMin();
		assertEquals("B", pq.min());
		pq.removeMin();
		assertEquals("C", pq.min());
		pq.removeMin();
		assertEquals("D", pq.min());
		pq.removeMin();
	}
	
	@Test
	public void testSize() throws Exception {
		
		PriorityQueue<Integer,String> pq = new ArrayListPQ<>();
		pq.insert(1, "B");
		pq.insert(5, "D");
		pq.insert(4, "C");
		pq.insert(-1, "A");
		
		assertEquals(4, pq.size());
		assertFalse(pq.isEmpty());
		
		assertEquals("A", pq.removeMin());
		assertEquals(3, pq.size());
		assertFalse(pq.isEmpty());
		
		assertEquals("B", pq.removeMin());
		assertEquals(2, pq.size());
		assertFalse(pq.isEmpty());
		
		assertEquals("C", pq.removeMin());
		assertEquals(1, pq.size());
		assertFalse(pq.isEmpty());
		
		assertEquals("D", pq.removeMin());
		assertEquals(0, pq.size());
		assertTrue(pq.isEmpty());
		
	}
}
