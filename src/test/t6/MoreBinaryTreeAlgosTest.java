package t6;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.LinkedList;

import org.junit.Before;
import org.junit.Test;

import t4.binarytree.BTNode;
import t4.binarytree.SimpleBTNode;
import t4.binarytree.SimpleBinaryTree;
import t5.BinaryTreeAlgos;

public class MoreBinaryTreeAlgosTest {

	SimpleBinaryTree<Integer> t; 
	BTNode<Integer> five = new SimpleBTNode<>(5);
	BTNode<Integer> three = new SimpleBTNode<>(3, five);
	BTNode<Integer> seven = new SimpleBTNode<>(7, five);
	BTNode<Integer> two = new SimpleBTNode<>(2, three);
	BTNode<Integer> four = new SimpleBTNode<>(4, three);
	BTNode<Integer> six = new SimpleBTNode<>(6, seven);
	BTNode<Integer> eleven = new SimpleBTNode<>(11, seven);
	BTNode<Integer> nine = new SimpleBTNode<>(9, eleven);
	BTNode<Integer> ten = new SimpleBTNode<>(10, eleven);

	@Before
	public void setup() {

		five.setLeft(three);
		five.setRight(seven);
		three.setLeft(two);
		three.setRight(four);

		seven.setLeft(six);
		seven.setRight(eleven);
		eleven.setLeft(nine);
		eleven.setRight(ten);

		t = new SimpleBinaryTree<>();
		t.setRoot(five);
	}
	
	@Test
	public void testStackBasedInorder() throws Exception {
		LinkedList<Integer> res = new LinkedList<>(); 
		MoreBinaryTreeAlgos.stackBasedInorder(new SimpleBinaryTree<Integer>(), res::add);
		assertEquals(Arrays.asList(), res);
		
		MoreBinaryTreeAlgos.stackBasedInorder(t, res::add);
		assertEquals(Arrays.asList(2,3,4,5,6,7,9,11,10), res);
		
	}
	
	@Test
	public void testLevelOrderTraversal() throws Exception {
		class Tuple{
			int pos;
			int val;
			
			Tuple(int pos, int val){
				this.pos = pos;
				this.val = val;
			}
			
			@Override
			public boolean equals(Object obj) {
				if(obj == this) return true;
				if(! (obj instanceof Tuple)) return false;
				Tuple to = (Tuple)obj;
				return this.pos == to.pos && this.val == to.val;
			}
			
			@Override
			public String toString() {
				return "(" + pos + "," + val + ")";
			}	
		}
		final int[] currPos = new int[] {1};
		LinkedList<Tuple> res = new LinkedList<>(); 
		MoreBinaryTreeAlgos.levelOrderTraversal(t, 
				e -> { res.add(new Tuple(currPos[0]++, e)); }, 
				e -> { currPos[0]++; });
		
		assertEquals(
				Arrays.asList(
						new Tuple(1, 5),
						new Tuple(2, 3),
						new Tuple(3, 7),
						new Tuple(4, 2),
						new Tuple(5, 4),
						new Tuple(6, 6),
						new Tuple(7, 11),
						new Tuple(14, 9),
						new Tuple(15, 10)), res);
				
	}
	
	@Test
	public void testEulerRemoveLeaf() throws Exception {
		MoreBinaryTreeAlgos.eulerTraversal(t,
				n -> { 
					if(!n.hasLeft() && !n.hasRight()) {
						if(t.getRoot() == n) t.setRoot(null);
						else n.getParent().removeChild(n);
					}}
				,n -> {}, n -> {});

			assertEquals("5(3, 7(, 11))", BinaryTreeAlgos.toParentheticString(t));
	}
}
