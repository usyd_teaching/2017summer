package t12;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.junit.Test;

public class DuplicateDetectorTest {

	@Test
	public void testEmpty() throws Exception {
		assertThat(DuplicateDetector.removeDuplicates(Collections.emptyList()).isEmpty(), is(true) );
		assertThat(DuplicateDetector.findDuplicates(Collections.emptyList()).isEmpty(), is(true) );
	}
	
	@Test
	public void testRemoveDuplicates_ThreeOnes() throws Exception {
		List<Integer> res = DuplicateDetector.removeDuplicates(Arrays.asList(1,1,1));
		assertThat(res.size(), is(1));
		assertThat(res, hasItem(1));
	}		
	
	@Test
	public void testRemoveDuplicates_TwoDups() throws Exception {
		assertThat(DuplicateDetector.removeDuplicates(Arrays.asList(1,2,2,4)), hasItems(1,2,4));
	}
	

	@Test
	public void testFindDuplicates_ThreeOnes() throws Exception {
		List<Integer> res = DuplicateDetector.findDuplicates(Arrays.asList(1,1,1));
		assertEquals(Arrays.asList(1,1), res);
	}		
	
	@Test
	public void testFindDuplicates_TwoDups() throws Exception {
		assertEquals(Arrays.asList(2), DuplicateDetector.findDuplicates(Arrays.asList(1,2,2,4)));
	}

	@Test
	public void testFindDuplicates_ThreeDups() throws Exception {
		assertEquals(Arrays.asList(2,3,3), DuplicateDetector.findDuplicates(Arrays.asList(1,2,2,3,3,3)));
	
	}

}
