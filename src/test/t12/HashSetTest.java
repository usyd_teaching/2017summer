package t12;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import org.junit.Test;

public class HashSetTest {

	@Test
	public void testConstruction() throws Exception {
		HashSet<String> h = new HashSet<>();
		assertTrue(h.isEmpty());
		assertEquals(0, h.size());
	}
	
	@Test
	public void testSize() throws Exception {
		HashSet<String> h = new HashSet<>();
		h.add("A");
		assertEquals(1, h.size());
		h.add("B");
		assertEquals(2, h.size());
		h.add("C");
		assertEquals(3, h.size());
	}
	
	@Test
	public void testContains() throws Exception {
		
		HashSet<String> h = new HashSet<>();
		h.add("A");
		h.add("B");
		h.add("C");
		
		assertThat(h.contains("A"), is(true) );
		assertThat(h.contains("B"), is(true) );
		assertThat(h.contains("C"), is(true) );
		assertThat(h.contains("D"), is(false) );
		assertThat(h.contains("E"), is(false) );
		
		h.remove("A");
		assertThat(h.contains("A"), is(false));
		h.remove("C");
		assertThat(h.contains("C"), is(false));
		h.remove("B");
		assertThat(h.contains("B"), is(false));
		
	}
	
	@Test
	public void testGetMembers() throws Exception {
		
		HashSet<String> h = new HashSet<>();
		h.add("A");
		h.add("B");
		h.add("C");
		
		assertThat(h.getMembers(), hasItems("A","B","C"));
	}

	@Test
	public void testDuplicateGetMembers() throws Exception {
		
		HashSet<String> h = new HashSet<>();
		h.add("A");
		h.add("B");
		h.add("C");
		
		h.add("A");
		h.add("B");
		assertThat(h.getMembers(), hasItems("A","B","C"));
	}
	
	@Test
	public void testUnion() throws Exception {
		
		HashSet<String> h = new HashSet<>();
		h.union(new HashSet<>());
		assertThat(h.getMembers().isEmpty(), is(true));
		h.add("A");
		h.union(new HashSet<>());
		assertThat(h.getMembers(), hasItem("A"));
		
		HashSet<String> h2 = new HashSet<>();
		h2.add("B");
		h.union(h2);
		assertThat(h.getMembers(), hasItems("A","B"));
	}
	
	@Test
	public void testDifference() throws Exception {

//	    Creates an empty HashSet of size 10
//	    Ensures that calling difference() with an empty set results in getMembers() returning an empty list
//	    Adds the numbers 5, 3 and 7 to the set
//	    Creates a new HashSet of size 10 containing the numbers 5, 7 and 11
//	    Ensures that calling difference() with this new HashSet results in getMembers() returning a list containing only 3
		HashSet<Integer> h = new HashSet<>();
		h.union(new HashSet<>());
		assertThat(h.getMembers().isEmpty(), is(true));
		h.add(5);
		h.add(3);
		h.add(7);
	    
		HashSet<Integer> h2 = new HashSet<>();
		h2.add(5);
		h2.add(7);
		h2.add(11);
		
		h.difference(h2);
		assertThat(h.getMembers(), hasItem(3));
	}
	
	@Test
	public void testIntersection() throws Exception {

//	    Creates an empty HashSet of size 10
//	    Ensures that calling intersection() with an empty set results in getMembers() returning an empty list
//	    Adds the numbers 5, 3 and 7 to the set
//	    Creates a new HashSet of size 10 containing the numbers 5, 7 and 11
//	    Ensures that calling intersection() with this new HashSet results in getMembers() returning a list containing 5 and 7 (in any order)

		
		HashSet<Integer> h = new HashSet<>();
		h.intersection(new HashSet<>());
		assertThat(h.getMembers().isEmpty(), is(true));
		
		h.add(5);
		h.add(3);
		h.add(7);
	    
		HashSet<Integer> h2 = new HashSet<>();
		h2.add(5);
		h2.add(7);
		h2.add(11);
		
		h.intersection(h2);
		assertThat(h.getMembers(), hasItems(5,7));
	}
	
}
