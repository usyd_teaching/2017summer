package t5;

import java.util.LinkedList;
import java.util.List;

import t4.binarytree.BTNode;
import t4.binarytree.BinaryTree;

public class BinaryTreeAlgos {

	public static <T> String toParentheticString(BinaryTree<T> t) {
		StringBuffer buff = new StringBuffer();
		return parenthesize(t.getRoot(), buff).toString();
	}
	
	private static <T> StringBuffer parenthesize(BTNode<T> node, StringBuffer buff) {
		if(node == null) return buff;
		buff.append(node.getElement().toString());
		if(node.hasLeft() || node.hasRight()) {
			buff.append('(');
			if(node.hasLeft()) { parenthesize(node.getLeft(), buff); }
			buff.append(',');
			if(node.hasRight()) { buff.append(' '); parenthesize(node.getRight(), buff); }
			buff.append(')');
		}
		return buff;
	}
	
	public static <T> int countLeaves(BinaryTree<T> t) {
		if(t.isEmpty()) return 0;
		
		return countLeaves(t.getRoot());
	}
	
	private static <T> boolean isLeaf(BTNode<T> node) {
		if(node == null) return false;
		return !(node.hasLeft() || node.hasRight());
	}
	
	private static <T> int countLeaves(BTNode<T> node) {
		if(node == null) return 0;
		if(isLeaf(node)) return 1;
		return countLeaves(node.getLeft()) + countLeaves(node.getRight());
	}
	
	
	public static <T> boolean twoChildrenOrNone(BinaryTree<T> t) {
		if(t.isEmpty()) return false;
		return twoChildrenOrNone(t.getRoot());
	}
	
	private static <T> boolean twoChildrenOrNone(BTNode<T> node) {
		if( isLeaf(node) ) return true;
		
		if( isCompleteNode(node)) {
			return twoChildrenOrNone(node.getLeft()) 
					&& twoChildrenOrNone(node.getRight());
		}
		return false;
	}
	
	private static <T> boolean isCompleteNode(BTNode<T> node) {
		if(node == null) return false;
		return node.hasLeft() && node.hasRight(); 
	}
	
	public static <T> void trim(BinaryTree<T> t) {
		if(t.isEmpty()) return;
		
		trim(t, t.getRoot());
	}
	
	private static <T> void trim(BinaryTree<T> t,BTNode<T> node) {
		if(isLeaf(node)) {
			t.remove(node);
		}else {
			trim(t, node.getLeft());
			trim(t, node.getRight());
		}
		// assuming newly created leaf nodes are left alone
		// otherwise just have another check if this node is leaf and remove accordingly.
	}
	
	@SuppressWarnings("rawtypes")
	public static class Coordinate {
		// Note: I could have used java.awt.Point instead
		int x,y;
		BTNode node;

		public Coordinate(int x, int y, BTNode node){
			this.x = x;
			this.y = y;
			this.node = node;
		}
		
		@Override
		public boolean equals(Object o) {
			if(o == this) return true;
			if(o instanceof Coordinate) {
				Coordinate c = (Coordinate) o;
				return c.x == this.x && c.y == this.y && c.node == this.node;
			}
			return false;
		}
		
		@Override
		public String toString() {
			return "(" + x + ", " + y + " : " + node.getElement() + ")";
		}
	}
	/**
	 *  Each node has a pair of (x; y) coordinates in the plane. 
	 *  The x-axis represents the order produced by inorder traversal. 
	 *  The y-axis is the height, i.e., the y coordinate is the height of a node in T. 
	 *  This method computes the node coordinates. 
	 */
	public static <T> List<Coordinate> getNodeCoordinates(BinaryTree<T> t) {
		LinkedList<Coordinate> coords = new LinkedList<>();
		if(t.isEmpty()) return coords;
		nodeCoordinate(t.getRoot(), 1, 0, coords);
		return coords;
	}
	
	/**
	 * 
	 * @param node
	 * @param height
	 * @return next inorderCoordinateX
	 */
	private static <T> int nodeCoordinate(BTNode<T> node, int inorderCoordinateX, int height, List<Coordinate> res) {
		if(node == null) { return inorderCoordinateX; }
		int myCoordX = nodeCoordinate(node.getLeft(), inorderCoordinateX, height+1, res);
		res.add(new Coordinate(myCoordX, height, node));
		return nodeCoordinate(node.getRight(), myCoordX+1, height+1, res);
	}
	
}
