package t4;

import java.util.List;

public interface Node<T> {
	public T getElement();

	public void setElement(T element);

	public Node<T> getParent();

	public void setParent(Node<T> parent);

	public List<Node<T>> getChildren();

	public boolean addChild(Node<T> child);

	public boolean removeChild(Node<T> child);

}
