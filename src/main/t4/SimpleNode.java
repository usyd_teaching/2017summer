package t4;

import java.util.LinkedList;
import java.util.List;

public class SimpleNode<T> implements Node<T> {

	T element;
	Node<T> parent;
	List<Node<T>> children;
	
	public SimpleNode() {
		this(null, null);
	}
	
	public SimpleNode(Node<T> parent) {
		this(parent,null);
	}
	
	public SimpleNode(T element){
		this(null,element);
	}

	public SimpleNode(Node<T> parent, T element){
		
		this.parent = parent;
		this.element = element;
		children = new LinkedList<>();
	}

	@Override
	public T getElement() {
		return element;
	}

	@Override
	public void setElement(T element) {
		this.element = element;
	}

	@Override
	public Node<T> getParent() {
		return parent;
	}

	@Override
	public void setParent(Node<T> parent) {
		this.parent = parent;
	}

	@Override
	public List<Node<T>> getChildren() {
		return children;
	}

	@Override
	public boolean addChild(Node<T> child) {
		if(children.add(child)) {
			child.setParent(this);
			return true;
		}
		return false;
	}

	@Override
	public boolean removeChild(Node<T> child) {
		if(children.remove(child)) {
			child.setParent(null);
			return true;
		}
		return false;
	}

}
