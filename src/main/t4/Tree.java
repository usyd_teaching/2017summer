package t4;

public interface Tree<T> {
	public int size();

	public boolean isEmpty();

	public void setRoot(Node<T> root);

	public Node<T> getRoot();

	public boolean insert(Node<T> parent, Node<T> child);

	public boolean remove(Node<T> node);
}
