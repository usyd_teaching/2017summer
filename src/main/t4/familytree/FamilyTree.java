package t4.familytree;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import t4.Node;
import t4.Tree;

public class FamilyTree {

	Tree<String> t;
	public FamilyTree(Tree<String> t) {
		this.t = t;
	}

	/**
	 * Returns the parent of the given node
	 */
	public String getParent(Node<String> person) {
		if(person.getParent() != null) 
			return person.getParent().getElement();
		
		return null;
	}

	/**
	 * Returns the parent of the parent of the given node
	 */
	public String getGrandparent(Node<String> person) {
		if(person.getParent() != null && person.getParent().getParent() != null) 
			return person.getParent().getParent().getElement();
		return null;
	}

	/**
	 * Returns all children of the given node
	 */
	public List<String> getChildren(Node<String> person){
		return person.getChildren().stream().map( Node::getElement ).collect(Collectors.toList());
	}

	/**
	 * Returns all children of the children of the given node
	 */
	public List<String> getGrandchildren(Node<String> person){
		return person.getChildren().stream()
				.flatMap( c -> { return c.getChildren().stream(); })
				.map (Node::getElement )
				.collect(Collectors.toList());
	}
	
	/**
	 * Returns all siblings of the given node
	 */
	public List<String> getSiblings(Node<String> person){
	
		if(person != null && person.getParent() != null) {
			List<Node<String>> tmp = new LinkedList<>(person.getParent().getChildren());
			tmp.remove(person);
			return tmp.stream().map(Node::getElement).collect(Collectors.toList());
			
		}
		return new LinkedList<>();
	}
	
}
