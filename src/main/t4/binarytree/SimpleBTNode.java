package t4.binarytree;

import java.util.LinkedList;
import java.util.List;

import t4.Node;

public class SimpleBTNode<T> implements BTNode<T>{

	T element;
	BTNode<T> parent, left, right;
	
	public SimpleBTNode(T element) {
		this.element = element;
	}
	
    public SimpleBTNode(T element, BTNode<T> parent) {
    	this.element = element;
    	this.parent = parent;
    }
    
	@Override
	public T getElement() {
		return element;
	}

	@Override
	public void setElement(T element) {
		this.element = element;
	}

	@Override
	public BTNode<T> getParent() {
		return parent;
	}

	@Override
	public void setParent(BTNode<T> node) {
		this.parent = node;
	}

	@Override
	public BTNode<T> getLeft() {
		return left;
	}

	@Override
	public BTNode<T> getRight() {
		return right;
	}

	@Override
	public void setLeft(BTNode<T> node) {
		this.left = node;
		node.setParent(this);
	}

	@Override
	public void setRight(BTNode<T> node) {
		this.right = node;
		node.setParent(this);
	}

	@Override
	public void setParent(Node<T> parent) {
		if(parent instanceof BTNode<?>)
			this.setParent((BTNode<T>) parent);
	}

	@Override
	public List<Node<T>> getChildren() {
		LinkedList<Node<T>> res = new LinkedList<>();
		res.add(left);
		res.add(right);
		return res;
	}

	@Override
	public boolean addChild(Node<T> child) {
		if(child instanceof BTNode<?>) {
			if(left == null) {
				setLeft((BTNode<T>) child);
				return true;
			}
			if(right == null) {
				setRight((BTNode<T>) child);
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean removeChild(Node<T> child) {
		if(child == left) {
			left = null;
			return true;
		}
		
		if(child == right) {
			right = null;
			return true;
		}
		
		return false;
	}

	@Override
	public boolean hasLeft() {
		return left != null;
	}

	@Override
	public boolean hasRight() {
		return right != null;
	}


}
