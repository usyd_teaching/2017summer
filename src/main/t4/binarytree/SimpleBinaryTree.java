package t4.binarytree;

import t4.Node;
import t4.SimpleTree;

public class SimpleBinaryTree<T> implements BinaryTree<T>{

	SimpleTree<T> t;
	
	public SimpleBinaryTree() {
		t = new SimpleTree<>();
	}
	
	@Override
	public int size() {
		return t.size();
	}

	@Override
	public boolean isEmpty() {
		return t.isEmpty();
	}

	@Override
	public void setRoot(Node<T> root) {
		if(root instanceof BTNode<?>) {
			setRoot((BTNode<T>)root);
		}
	}


	@Override
	public boolean insert(Node<T> parent, Node<T> child) {
		if(parent instanceof BTNode<?> && child instanceof BTNode<?>) {
			return insert((BTNode<T>)parent, (BTNode<T>)child);
		}
		return false;
	}

	@Override
	public boolean remove(Node<T> node) {
		if(node instanceof BTNode<?>) {
			return remove((BTNode<T>)node);
		}
		return false;
	}

	@Override
	public BTNode<T> getRoot() {
		return (BTNode<T>) t.getRoot();
	}

	@Override
	public boolean insert(BTNode<T> parent, BTNode<T> child) {
		return t.insert(parent, child);
	}

	@Override
	public boolean remove(BTNode<T> node) {
		return t.remove(node);
	}


	@Override
	public void setRoot(BTNode<T> root) {
		t.setRoot(root);
	}

	
}
