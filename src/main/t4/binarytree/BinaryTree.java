package t4.binarytree;

import t4.Tree;

public interface BinaryTree<T> extends Tree<T>{
	
	public BTNode<T> getRoot();
	public void setRoot(BTNode<T> root);
	
	public boolean insert(BTNode<T> parent, BTNode<T> child);
	public boolean remove(BTNode<T> node);
}
