package t4.binarytree;

import t4.Node;

public interface BTNode<T> extends Node<T>{
	
	public T getElement();

	public void setElement(T element);

	public BTNode<T> getParent();
	public void setParent(BTNode<T> node);

	public boolean hasLeft();
	public boolean hasRight();
	
	public BTNode<T> getLeft();
	public BTNode<T> getRight();
	public void setLeft(BTNode<T> node);
	public void setRight(BTNode<T> node);
}
