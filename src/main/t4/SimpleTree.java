package t4;

public class SimpleTree<T> implements Tree<T>{
	
	int size = 0;
	Node<T> root;

	@Override
	public int size() {
		return size;
	}

	@Override
	public boolean isEmpty() {
		return size == 0;
	}

	@Override
	public void setRoot(Node<T> root) {
		this.root = root;
		size = size(root);
	}

	@Override
	public Node<T> getRoot() {
		return root;
	}

	@Override
	public boolean insert(Node<T> parent, Node<T> child) {
		if(parent.addChild(child)) {
			size++;
			return true;
		}
		return false;
	}

	@Override
	public boolean remove(Node<T> node) {
		//find node in tree
		// what traversal shall we use?
		if(root == node) {
			root = null;
			size = 0;
			return true;
		}
		
		if(remove(root, node)) {
			// need to determine size of subtree removed;
			size -= size(node);
		}
		
		return false;
	}
	
	private boolean remove(Node<T> curr, Node<T> toRemove) {
		if(curr == null) return false;
		if(curr.removeChild(toRemove)) {
			return true;
		}else {
			for(Node<T> child: curr.getChildren()) {
				boolean removed = remove(child, toRemove);
				if(removed) return true;
			}
		}
		return false;
		
	}
	
	/**
	 * Return size of subtree rooted at node
	 * @param node
	 * @return
	 */
	private int size(Node<T> node) {
		if(node == null) return 0;
		int size = 1;
		for(Node<T> child: node.getChildren()) {
			size += size(child);
		}
		return size;
	}

}
