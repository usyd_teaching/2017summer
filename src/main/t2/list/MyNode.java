package t2.list;

public class MyNode<T> implements Node<T> {

	T element;
	Node<T> next;
	
	public MyNode(T element) {
		this(element, null);
	}
	
	public MyNode(T element, MyNode<T> next) {
		this.element = element;
		this.next = next;
	}
	
	@Override
	public T getElement() {
		return element;
	}

	@Override
	public void setElement(T element) {
		this.element = element;
	}

	@Override
	public Node<T> getNext() {
		return next;
	}

	@Override
	public void setNext(Node<T> next) {
		this.next = next;
	}

}
