package t2.list;

public class MyLinkedList<T> implements List<T>{

	Node<T> head; 
	int size = 0;
	
	@Override
	public Node<T> getHead() {
		return head;
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public boolean isEmpty() {
		return size == 0;
	}

	@Override
	public T get(int i) {
		if(i< 0 || i >= size) return null;
		Node<T> tmp = head;
		for(int idx=0; idx<i; idx++) {
			tmp = tmp.getNext();
		}
		return tmp.getElement();
	}

	@Override
	public void add(T o) {
		if(head == null) {
			head = new MyNode<>(o);
		}else {
			Node<T> curr = head;
			while(curr.getNext() != null) curr = curr.getNext();
			
			MyNode<T> newNode = new MyNode<>(o);
			curr.setNext(newNode);
		}
		
		size++;
		
	}

	@Override
	public void remove(T o) {
		// o can be null
		Node<T> curr = head;
		
		if(head.getElement() == o) {
			head = head.getNext();
			size--;
			return;
		}
		
		while(curr != null && curr.getNext() != null && curr.getNext().getElement() != o) {
			curr = curr.getNext();
		}
		
		if(curr == null || curr.getNext() == null) return;
		

		else{ // element found also implies that (curr.getNext() != null) {
			Node<T> tmp = curr.getNext();
			curr.setNext(curr.getNext().getNext());
			tmp.setNext(null);
			size--;
		}

	}

}
