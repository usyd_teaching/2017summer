package t2.list;

public interface Node<T> {
	public T getElement();

    public void setElement(T element);

    public Node<T> getNext();

    public void setNext(Node<T> next);
}
