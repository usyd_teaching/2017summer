package t2.list;

public interface List<T> {
	public Node<T> getHead();

	public int size();
	public boolean isEmpty();

	public T get(int i);

	public void add(T o);
	public void remove(T o);
}
