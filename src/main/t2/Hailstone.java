package t2;

import java.util.ArrayList;
import java.util.List;

public class Hailstone {

	public static List<Integer> hailstone(int n){
		return efficientHs(n, new ArrayList<>());
	}
	
	private static List<Integer> efficientHs(int n, List<Integer> res){
		res.add(n);
		if(n==1) return res;
		if(n%2 == 0) return efficientHs(n/2, res);
		else return efficientHs(3*n+1, res);
	}
}
