package t3;

import java.util.Stack;

public class Fibonacci {

	public static Stack<Integer> getNumbers(int n){
		
		Stack<Integer> res = new Stack<>();
		if(n < 0) return res;
		
		res.push(0); 
		if(n==0) return res;
		
		res.push(1);
		for(int i=2; i <= n; i++) {
			int fn_1 = res.pop();
			int fn_2 = res.pop();
			
			int fn = fn_1 + fn_2;
			
			res.push(fn_2);
			res.push(fn_1);
			res.push(fn);
			
		}
		return res;
	}
}
