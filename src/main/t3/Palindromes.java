package t3;

import java.util.Stack;

public class Palindromes {

	public static boolean isPalindrome(String word) {
//		return isPalindromeIterative(word);
//		return isPalindromeRecursive(word);
		return isPalindromeStack(word);
	}

	public static boolean isPalindromeSentence(String sentence) {
		StringBuilder buff = new StringBuilder();
		for(char c: sentence.toCharArray()) {
			if(Character.isAlphabetic(c)) buff.append(Character.toLowerCase(c));
		}
		
		return isPalindrome(buff.toString());
	}
	
	private static boolean isPalindromeIterative(String oriword) {
		char[] word = oriword.toCharArray();
		int startIdx = 0, endIdx = word.length-1;
		while(startIdx < endIdx) {
			if(word[startIdx++] != word[endIdx--]) return false;
		}
		return true;
	}
	
	private static boolean isPalindromeRecursive(String word) {
		return isPalindromeRecursive(0, word.length()-1, word.toCharArray());

	}
	private static boolean isPalindromeRecursive(int startIdx, int endIdx, char[] word) {
		if(startIdx >= endIdx) return true;
		return (word[startIdx] == word[endIdx]) && isPalindromeRecursive(startIdx+1, endIdx-1, word);
	}
	
	private static boolean isPalindromeStack(String word) {
		Stack<Character> chars = new Stack<>();
		for(char c: word.toCharArray()) chars.push(c);
		
		StringBuilder buff  = new StringBuilder();
		
		while(!chars.isEmpty()) {
			buff.append(chars.pop().charValue());
		}
		return buff.toString().equals(word);
	}
}
