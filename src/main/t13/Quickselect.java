package t13;

import java.util.Arrays;


public class Quickselect {

	public static <T extends Comparable<T>> T quickselect(int k, T... vals) {
		if(vals == null || vals.length==0) throw new IllegalArgumentException("Null or empty values used as arguments");
		if(k<1 || k>vals.length) throw new IllegalArgumentException("Invalid value for argument k");
		T[] copy = Arrays.copyOf(vals, vals.length);
//		Arrays.sort(copy);
//		return copy[k-1];
		return quickselect(k, 0, copy.length-1, copy);
			
	}
	
	private static <T extends Comparable<T>> T quickselect(int k, int left, int right, T... vals) {
		k = k-1;
		while(true) {
			
			if(left == right) return vals[left];
			int pivotIdx = left + (int)(Math.random()*(right - left + 1));
			int partIdx = partition(left, right, pivotIdx, vals);
			if(partIdx == k) return vals[partIdx];
			else if(k < partIdx) right = partIdx-1;
			else left = partIdx+1;
		}
	}
	
	private static <T extends Comparable<T>> int partition(int left, int right, int pivotIdx, T... vals) {
		T pivot = vals[pivotIdx];
		T rightT = vals[right];
		vals[right] = pivot; vals[pivotIdx] = rightT; //swap
		
		int storeIdx = left;
		for(int i=left; i<right; i++) {
			if(vals[i].compareTo(pivot) < 0 ) {
				T storeT = vals[storeIdx];
				T iT = vals[i];
				vals[i] = storeT; vals[storeIdx] = iT; //swap
				storeIdx++;
			}
		}
		T storeT = vals[storeIdx];
		vals[storeIdx] = pivot;  //move  pivot to right place
		vals[right] = storeT; 
		return storeIdx;
	}
}
