package t1;


public class EnglishCalculator {

	public static double calculate(String s){
		String[] fields = s.split("\\s+");
		int a = Integer.parseInt(fields[0]);
		int b = Integer.parseInt(fields[fields.length-1]);
		switch(fields[1].toLowerCase()){
		case "plus":
			return SimpleCalculator.add(a, b);
		case "minus":
			return SimpleCalculator.subtract(a, b);
		case "times":
			return SimpleCalculator.multiply(a, b);
		case "divided":
			return SimpleCalculator.divide(a, b);
		default:
			throw new IllegalArgumentException();
		}
	}
}
