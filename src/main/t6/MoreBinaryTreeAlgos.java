package t6;

import java.util.LinkedList;
import java.util.List;
import java.util.Stack;
import java.util.function.Consumer;

import t4.Node;
import t4.binarytree.BTNode;
import t4.binarytree.BinaryTree;

public class MoreBinaryTreeAlgos {

	
	public static <E> void inorder(BinaryTree<E> tree, Consumer<E> visitor) {
		inorder(tree.getRoot(), visitor);
	}
	
	private static <E> void inorder(BTNode<E> node, Consumer<E> visitor) {
		if(node == null) return;
		inorder(node.getLeft(), visitor);  // recursively traverse left-branch
		visitor.accept(node.getElement()); // visit this node
		inorder(node.getRight(), visitor); // recursively traverse right-branch
	}
	
	public static <E> void postorder(BinaryTree<E> tree, Consumer<E> visitor) {
		postorder(tree.getRoot(), visitor);
	}
	
	private static <E> void postorder(BTNode<E> node, Consumer<E> visitor) {
		if(node == null) return;
		inorder(node.getLeft(), visitor);  // recursively traverse left-branch
		inorder(node.getRight(), visitor); // recursively traverse right-branch
		visitor.accept(node.getElement()); // visit this node
	}

	public static <E> void preorder(BinaryTree<E> tree, Consumer<E> visitor) {
		preorder(tree.getRoot(), visitor);
	}
	
	private static <E> void preorder(BTNode<E> node, Consumer<E> visitor) {
		if(node == null) return;
		visitor.accept(node.getElement()); // visit this node
		inorder(node.getLeft(), visitor);  // recursively traverse left-branch
		inorder(node.getRight(), visitor); // recursively traverse right-branch
	}

	public static <E> void stackBasedInorder(BinaryTree<E> tree, Consumer<E> visitor) {
		if(tree==null || tree.isEmpty()) return;
		if(visitor == null) throw new IllegalArgumentException();

		class NodeState{
			BTNode<E> node;
			boolean skipLeft;
			
			NodeState(BTNode<E> node){
				this.node = node;
				this.skipLeft = false;
			}
			
			NodeState setSkipLeft() {
				skipLeft = true;
				return this;
			}
		}
		
		Stack<NodeState> runstack = new Stack<>();
		
		runstack.push(new NodeState(tree.getRoot()));
		while(!runstack.isEmpty()) {
			NodeState ns = runstack.pop();
			BTNode<E> node = ns.node;
			if(node != null) {
				if(ns.node.hasLeft() && !ns.skipLeft) {
					runstack.push(ns.setSkipLeft());
					runstack.push(new NodeState(node.getLeft())); 
					continue;
				}
				
				visitor.accept(node.getElement());
				
				if(node.hasRight()) {
					runstack.push(new NodeState(node.getRight()));
				}
			}
		}
	}

	/**
	 * Performs a level-order traversal. 
	 * 
	 * @param bt - binary tree to traverse
	 * @param visitor - called when 'visiting' a node of bt
	 * @param emptyVisitor - called during level-order traversal at an 'empty'-spot of a complete binary tree; argument passed to visitor will be null.
	 * 
	 */
	public static <E> void levelOrderTraversal(BinaryTree<E> bt, Consumer<E> visitor, Consumer<E> emptyVisitor) {
		if(bt==null || bt.isEmpty()) return;
		if(visitor == null) throw new IllegalArgumentException();
		
		LinkedList<BTNode<E>> queue = new LinkedList<>();
		BTNode<E> EMPTY_NODE = new BTNode<E>() {

			@Override
			public void setParent(Node<E> parent) {}

			@Override
			public List<Node<E>> getChildren() {
				return null;
			}

			@Override
			public boolean addChild(Node<E> child) {
				return false;
			}

			@Override
			public boolean removeChild(Node<E> child) {
				return false;
			}

			@Override
			public E getElement() {
				return null;
			}

			@Override
			public void setElement(E element) {
				
			}

			@Override
			public BTNode<E> getParent() {
				return null;
			}

			@Override
			public void setParent(BTNode<E> node) {
			}

			@Override
			public boolean hasLeft() {
				return false;
			}

			@Override
			public boolean hasRight() {
				return false;
			}

			@Override
			public BTNode<E> getLeft() {
				return null;
			}

			@Override
			public BTNode<E> getRight() {
				return null;
			}

			@Override
			public void setLeft(BTNode<E> node) {
			}

			@Override
			public void setRight(BTNode<E> node) { }
		};
		
		queue.add(bt.getRoot());
		int numNodesVisited = 0, numNodesExpected = bt.size();
		
		while(!queue.isEmpty()) {
			BTNode<E> node = queue.remove();

			if(node == EMPTY_NODE) emptyVisitor.accept(null);
			else {
				visitor.accept(node.getElement());
				numNodesVisited++;
			}

			if(numNodesVisited < numNodesExpected || node.hasLeft() || node.hasRight()) {
				if(node.hasLeft()) queue.add(node.getLeft());
				else queue.add(EMPTY_NODE);
				
				if(node.hasRight()) queue.add(node.getRight());
				else queue.add(EMPTY_NODE);
			}
		}
	}
	
	private enum EulerState{
		LEFT_VISIT,
		BOTTOM_VISIT,
		RIGHT_VISIT
	}

	public static <E> void eulerTraversal(BinaryTree<E> bt, Consumer<BTNode<E>> leftVisit, Consumer<BTNode<E>> bottomVisit, Consumer<BTNode<E>> rightVisit) {
				
		if(bt == null || bt.isEmpty()) return;
		
		class State{
			BTNode<E> node;
			EulerState currState;
			
			State(BTNode<E> node, EulerState state){
				this.node = node;
				this.currState = state;
			}
			
			State setState(EulerState state) {
				currState = state;
				return this;
			}
		}
		
		Stack<State> runStack = new Stack<>();
		
		runStack.push(new State(bt.getRoot(), EulerState.LEFT_VISIT));
		
		while(!runStack.isEmpty()) {
			State st = runStack.pop();

			switch(st.currState) {

			case LEFT_VISIT:
				leftVisit.accept(st.node);
				runStack.push(st.setState(EulerState.BOTTOM_VISIT));
				if(st.node.hasLeft()) {
					runStack.push(new State(st.node.getLeft(), EulerState.LEFT_VISIT));
				}
				break;
			case BOTTOM_VISIT:
				bottomVisit.accept(st.node);
				runStack.push(st.setState(EulerState.RIGHT_VISIT));
				if(st.node.hasRight()) {
					runStack.push(new State(st.node.getRight(), EulerState.LEFT_VISIT));
				}
				break;
			case RIGHT_VISIT:
				rightVisit.accept(st.node);
				break;
			default:
				break;
			}
		}
	}
}
