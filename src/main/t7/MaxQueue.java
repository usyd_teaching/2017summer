package t7;

public class MaxQueue<E extends Comparable<? super E>>{
	/*
	 * If we were to be consistent with java.util.PriorityQueue, <E>
	 * would not extend Comparable to allow for a Comparator to be provided.
	 * 
	 * Here, <E> extends Comparable to make this adapter class simpler to understand.
	 * 
	 */

	java.util.PriorityQueue<E> pq = new java.util.PriorityQueue<>(
			(e1,e2) -> e2.compareTo(e1));  // provide a Comparator that reverses the ordering
	
	public void add(E el) {
		pq.add(el);
	}

	public E max() {
		return pq.peek();
	}
	
	public E removeMax() {
		return pq.remove();
	}
	
	public int size() {
		return pq.size();
	}
	
	public boolean isEmpty() {
		return pq.isEmpty();
	}
}
