package t7;

public class PQFIFO<E> {

	class Item implements Comparable<Item>{
		int order;
		E elem;
		
		Item(int order, E elem){
			this.order = order;
			this.elem = elem;
		}

		@Override
		public int compareTo(Item o) {
			return Integer.compare(this.order, o.order);
		}
	}
	
	java.util.PriorityQueue<Item> pq = new java.util.PriorityQueue<>();
	int order = Integer.MIN_VALUE;
	
	public void enqueue(E e) {
		pq.add(new Item(order++, e));
	}

	public E dequeue() {
		Item i = pq.remove();
		E res = null;
		if(i != null) {
			res = i.elem;
			if(pq.isEmpty()) order = Integer.MIN_VALUE;
		}
		return res;
	}

	public int size() {
		return pq.size();
	}

	public boolean isEmpty() {
		return pq.isEmpty();
	}
}
