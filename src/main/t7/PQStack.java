package t7;

import java.util.PriorityQueue;

public class PQStack<E> {

	class Item implements Comparable<Item>{
		int order;
		E elem;
		
		Item(int order, E elem){
			this.order = order;
			this.elem = elem;
		}

		@Override
		public int compareTo(PQStack<E>.Item o) {
			return Integer.compare(this.order, o.order);
		}
	}
	
	java.util.PriorityQueue<Item> pq = new PriorityQueue<>();
	int order = Integer.MAX_VALUE;
	
	public void push(E e) {
		pq.add(new Item(order--, e));
	}
	
	public E pop() {
		Item i = pq.remove();
		E res = null;
		if(i != null) {
			res = i.elem;
			if(pq.isEmpty()) order = Integer.MAX_VALUE;
		}
		return res;
	}
	
	public int getSize() {
		return pq.size();
	}
	
	public boolean isEmpty() {
		return pq.isEmpty();
	}
}
