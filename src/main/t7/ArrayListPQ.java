package t7;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Optional;

public class ArrayListPQ<K extends Comparable<? super K>,V> implements PriorityQueue<K,V> {

	class PQEntry implements Comparable<PQEntry>{
		K key;
		V val;
		
		PQEntry(K k, V v){
			key = k;
			val = v;
		}

		@Override
		public int compareTo(PQEntry o) {
			if(o == this) return 0;
			return this.key.compareTo(o.key);
		}
	}
	
	ArrayList<PQEntry> pq = new ArrayList<>();
	boolean sorted = false;
	
	@Override
	public void insert(K key, V value) {
		pq.add(new PQEntry(key, value));
		PQEntry prevLast = pq.get(pq.size() - 1);
		if(sorted && prevLast.key.compareTo(key) <= 0) {
			sorted = true;
		}else {
			sorted = false;
		}
	}

	private Optional<PQEntry> getMin() {
		
		if(!sorted) {
			Collections.sort(pq);
			sorted = true;
		}
		PQEntry v = null;
		if(pq.size() > 0) {
			v = pq.get(0);
		}
		return Optional.ofNullable(v);
	}

	PQEntry NULL_ENTRY = new PQEntry(null, null);
	@Override
	public V removeMin() {
		PQEntry min = getMin().orElse(NULL_ENTRY);
		
		if(min != NULL_ENTRY) pq.remove(min);
		return min.val;
	}

	
	@Override
	public V min() {
		PQEntry min = getMin().orElse(NULL_ENTRY);
		return min.val;
	}

	@Override
	public int size() {
		return pq.size();
	}

	@Override
	public boolean isEmpty() {
		return pq.isEmpty();
	}

}
