package t7;

public interface PriorityQueue<K extends Comparable<? super K>,V> {
	public void insert(K key, V value);
	public V removeMin();
	public V min();
	public int size();
	public boolean isEmpty();
}
