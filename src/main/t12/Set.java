package t12;

import java.util.List;

public interface Set<T> {

	public int size();

	public boolean isEmpty();

	public void add(T o);

	public void remove(T o);

	public boolean contains(T o);

	public List<T> getMembers();

	public void union(Set<T> s);

	public void intersection(Set<T> s);

	public void difference(Set<T> s);
    
}
