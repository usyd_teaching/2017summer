package t12;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class HashSet<T> implements Set<T> {

	HashMap<T,Boolean> map = new HashMap<>();
	
	@Override
	public int size() {
		return map.size();
	}

	@Override
	public boolean isEmpty() {
		return map.isEmpty();
	}

	@Override
	public void add(T o) {
		map.put(o, null);
	}

	@Override
	public void remove(T o) {
		map.remove(o);
	}

	@Override
	public boolean contains(T o) {
		return map.containsKey(o);
	}

	@Override
	public List<T> getMembers() {
		return new LinkedList<>(map.keySet());
	}

	@Override
	public void union(Set<T> s) {
		for(T t: s.getMembers()) map.put(t, null);
	}

	@Override
	public void intersection(Set<T> s) {
		getMembers().forEach((k) -> { 
			if(!s.contains(k)) map.remove(k);
		});
	}

	@Override
	public void difference(Set<T> s) {
		getMembers().forEach((k) -> { 
			if(s.contains(k)) map.remove(k);
		});
	}

}
