package t12;

import java.util.LinkedList;
import java.util.List;

public class DuplicateDetector {
	
	public static <T> List<T> removeDuplicates(List<T> l){
		Set<T> set = new HashSet<>();
		for(T t:l) set.add(t);
		return set.getMembers();
	}

	public static <T> List<T> findDuplicates(List<T> l){
		Set<T> set = new HashSet<>();
		List<T> res = new LinkedList<>();
		for(T t:l) {
			if(!set.contains(t)) set.add(t);
			else res.add(t);
		}
		return res;
	}
}
