package t11;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class TravelDestinations {
	DirectedGraph<String> g = new DirectedGraph<>();
	
   
	public void addFlight(String from, String to) {
		Node<String> nfrom = find(from);
		if(nfrom == null) {
			nfrom = new DirectedGraphNode<String>(from);
			
		}
		
		Node<String> nto = find(to);
		if(nto == null) {
			nto = new DirectedGraphNode<String>(to);
		}
		g.addEdge(nfrom, nto);
	}
	
    /**
     * Returns all the countries that are a single flight away from the given country, in any order
     */
    public List<String> getAvailableCountries(String fromCountry){
    	Node<String> n = find(fromCountry);
    	
    	if(n != null) {
    		return n.getNeighbours().stream()
    				.map(Node::getValue).collect(Collectors.toList());
    	}
    	return new LinkedList<>();
    }
    
    private Node<String> find(String country) {
    	
    	for(Node<String> n: g.getNodes()) {
    		if(n.getValue().equals(country)) {
    			return n;
    		}
    	}
    	return null;
    }
    
    /**
     * Returns whether there is a flight from 'fromCountry' to 'toCountry'
     */
    public boolean canFlyTo(String fromCountry, String toCountry) {
    	Node<String> from = find(fromCountry);
    	Node<String> to = find(toCountry);
    	
    	if(from == null || to == null) return false;
    
    	return from.getNeighbours().contains(to);
    	
    }
}
