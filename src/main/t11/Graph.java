package t11;

import java.util.List;

public interface Graph<T> {

        public int size();
        
        public boolean isEmpty();
        
        public List<Node<T>> getNodes();
        
        public void addNode(Node<T> n);
        
        public void removeNode(Node<T> n);
        
        public void addEdge(Node<T> source, Node<T> destination);
}
