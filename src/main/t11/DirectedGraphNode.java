package t11;

import java.util.LinkedList;
import java.util.List;

public class DirectedGraphNode<T> implements Node<T> {

	//based on adjacencylist
	private T value;
	private List<Node<T>> neighbours; //nodes directly reachable from this node
	
	public DirectedGraphNode(T value) {
		this.value = value;
		neighbours = new LinkedList<>();
	}
	
	@Override
	public void setValue(T value) {
		this.value = value;
	}

	@Override
	public T getValue() {
		return value;
	}

	@Override
	public List<Node<T>> getNeighbours() {
		return neighbours;
	}

	@Override
	public void addNeighbour(Node<T> n) {
		neighbours.add(n);
	}

	@Override
	public void removeNeighbour(Node<T> n) {
		neighbours.remove(n);
	}

}
