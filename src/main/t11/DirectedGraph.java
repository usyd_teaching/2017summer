package t11;

import java.util.LinkedList;
import java.util.List;

public class DirectedGraph<T> implements Graph<T> {

	List<Node<T>> nodes = new LinkedList<>();
	
	@Override
	public int size() {
		return nodes.size();
	}

	@Override
	public boolean isEmpty() {
		return nodes.isEmpty();
	}

	@Override
	public List<Node<T>> getNodes() {
		return nodes;
	}

	@Override
	public void addNode(Node<T> n) {
		nodes.add(n);
	}

	@Override
	public void removeNode(Node<T> n) {
		for(Node<T> node: nodes) {
			node.removeNeighbour(n);
		}
		nodes.remove(n);
	}

	@Override
	public void addEdge(Node<T> source, Node<T> destination) {
		if(!nodes.contains(source)) nodes.add(source);
		if(!nodes.contains(destination)) nodes.add(destination);
		
		source.addNeighbour(destination);
	}

}
