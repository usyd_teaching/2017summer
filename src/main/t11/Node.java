package t11;

import java.util.List;

public interface Node<T> {
    public void setValue(T value);
    
    public T getValue();
    
    public List<Node<T>> getNeighbours();
    
    public void addNeighbour(Node<T> n);
  
    public void removeNeighbour(Node<T> n);
}
